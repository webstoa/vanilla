import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VanillaSignupComponent } from './vanilla-signup.component';

describe('VanillaSignupComponent', () => {
  let component: VanillaSignupComponent;
  let fixture: ComponentFixture<VanillaSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VanillaSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VanillaSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
