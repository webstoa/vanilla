import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { VanillaViewDataService } from '../../services/vanilla-view-data.service';


@Component({
  selector: 'ws-vanilla-signup',
  templateUrl: './vanilla-signup.component.html',
  styleUrls: ['./vanilla-signup.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VanillaSignupComponent implements OnInit {

  constructor(private viewDataService: VanillaViewDataService) {
  }

  ngOnInit() {
  }

}
