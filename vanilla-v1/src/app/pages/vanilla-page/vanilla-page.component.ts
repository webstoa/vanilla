import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { VanillaArticleComponent } from '../../elements/vanilla-article/vanilla-article.component';
import { VanillaSignupComponent } from '../../elements/vanilla-signup/vanilla-signup.component';


@Component({
  selector: 'ws-vanilla-page',
  templateUrl: './vanilla-page.component.html',
  styleUrls: ['./vanilla-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class VanillaPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
