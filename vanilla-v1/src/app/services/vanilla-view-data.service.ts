import { Injectable } from '@angular/core';

@Injectable()
export class VanillaViewDataService {

  private _appTitle = 'Web Suite --- Vanilla V1';

  private _articleTitle = 'Placeholder';
  private _articleContentHTML = `<p><em>Coming Soon</em> - Under construction</p>`;


  constructor() { }


  get appTitle(): string {
    return this._appTitle;
  }

  get articleTitle(): string {
    return this._articleTitle;
  }

  get articleContentHTML(): string {
    return this._articleContentHTML;
  }

}
